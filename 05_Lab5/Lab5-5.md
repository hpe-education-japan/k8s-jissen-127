## Task 5: Secretを使う

**1) MySQL用Secretを定義します。リテラルで設定しましょう。**
```
$ kubectl create secret generic app-secret --from-literal=DB_host=mysql --from-literal=DB_user=root --from-literal=DB_Password=password
secret/app-secret created
```

**2) Secretを確認します。-o yamlで確認するとエンコードした値が表示されます。**
```
$ kubectl get secrets
NAME         TYPE     DATA   AGE
app-secret   Opaque   3      34s


$ kubectl describe secrets app-secret
Name:         app-secret
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
DB_host:      5 bytes
DB_user:      4 bytes
DB_Password:  7 bytes


$ kubectl get secrets app-secret -o yaml
apiVersion: v1
data:
  DB_Password: cGFzc3dvcg==
  DB_host: bXlzcWw=
  DB_user: cm9vdA==
kind: Secret
metadata:
  creationTimestamp: "2023-11-28T04:25:21Z"
  name: app-secret
  namespace: default
  resourceVersion: "41455"
  uid: 1d2502be-966c-48e5-9e3f-427e139050b8
type: Opaque

```

**3) Secretを使用するMYSQL Podを作成して、パスワードをSecretを使って受け渡します。マニフェストを作成しましょう。valueFromを使って受け渡します。**
```
$ vi db-pod.yaml
```

[db-pod.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/db-pod.yaml)

```
$ kubectl apply -f db-pod.yaml
deployment.apps/mysql-deployment created


$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
mysql-deployment-77d5d7d6c6-st54j   1/1     Running   0          17s
```

**4) 作成したMYSQL Podに接続して動作を確認してみましょう。**
```
$ kubectl exec -ti mysql-deployment-77d5d7d6c6-nx6b7 -- /bin/bash

bash-4.4# env
HOSTNAME=mysql-deployment-77d5d7d6c6-st54j
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
MYSQL_ROOT_PASSWORD=password
...


bash-4.4# mysql --password=password
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.2.0 MySQL Community Server - GPL

Copyright (c) 2000, 2023, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>


mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.01 sec)


mysql> exit
Bye


bash-4.4# exit

```

**5) DeploymentとSecretを削除しておきましょう。**
```
$ kubectl delete secret app-secret
secret "app-secret" deleted


$ kubectl delete deployment mysql-deployment
deployment.apps "mysql-deployment" deleted
```



<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Lab5-6.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-127)    
