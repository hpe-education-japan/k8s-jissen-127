## Task 3: PersistentVolumeとPersistentVolumeClaim 


この演習ではControl Plane Nodeにnfsサーバーをインストールして、それを使用してPVを作成します。  
<br>
<br>
![Lab5-2](/uploads/50aecb2a2a38f2fd7793994c9bb7cf81/Lab5-2.png)

<br>
<br>

**1) まず、コントロールプレーンノードにnfsサーバーをインストールします。**
```
$ sudo apt-get update && sudo apt-get install -y nfs-kernel-server
<省略>
```

**2) nfs で共有するためのディレクトリを作成し、テスト用ファイルを作成します。/tmp/ディレクトリと同じパーミッションを設定します。**
```
$ sudo mkdir /opt/sfw
$ sudo chmod 1777 /opt/sfw/
$ sudo bash -c 'echo software > /opt/sfw/hello.txt'

```

**3) nfsサーバの設定ファイル（/etc/exports）を編集し、新規に作成したディレクトリを共有するようにしましょう。ここでは、どこからでもディレクトリを共有できるようにします。**
```
$ sudo vi /etc/exports


以下を追加
/opt/sfw/ *(rw,sync,no_root_squash,subtree_check)

```

[exports](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/exports)


**4) /etc/exportsの設定の変更を反映します。**
```
$ sudo exportfs -ra
```

**5) ワーカーノードから共有ディレクトリをマウントできることを確認します。**
```
※ワーカーノードで実行※
student@setX-worker:~$ sudo apt-get -y install nfs-common
<省略>


student@setX-worker:~$ showmount -e k8scp
Export list for k8scp:
/opt/sfw *


student@setX-worker:~$ sudo mount k8scp:/opt/sfw /mnt


student@setX-worker:~$ ls -l /mnt
total 4
-rw-r--r-- 1 root root 9 Nov 28 11:24 hello.txt

```

**6) コントロールプレーンノードでPersistentVolume を作成するためのマニフェストを作成しましょう。コントロールプレーンノードのエイリアス名と先ほど作成したディレクトリを利用します。**
```
※コントロールプレーンノードで実行※
$ vi PVol.yaml
```

[PVol.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/PVol.yaml)

**7) PersistentVolume を作成しましょう。**
```
$ kubectl create -f PVol.yaml
persistentvolume/pvvol-1 created


$ kubectl get pv
NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Retain           Available                                   31s


$ kubectl describe pv pvvol-1
Name:            pvvol-1
Labels:          <none>
Annotations:     <none>
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:
Status:          Available
Claim:
Reclaim Policy:  Retain
Access Modes:    RWX
VolumeMode:      Filesystem
Capacity:        1Gi
Node Affinity:   <none>
Message:
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    k8scp
    Path:      /opt/sfw
    ReadOnly:  false
Events:        <none>

```

**8) PVC を作成するためのマニフェストを作成しましょう。**
```
$ vi pvc.yaml
```

[pvc.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/pvc.yaml)

**9) PersistentVolumeClaimを作成し、STATUS がBound になっていることを確認しましょう。サイズは200Mi を指定したにも関わらず、1Gi を利用しています。使用可能なPersistent Volume が1Gi のもののみだったためです。**
```
$ kubectl create -f pvc.yaml
persistentvolumeclaim/pvc-one created


$ kubectl get pvc
NAME      STATUS   VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-one   Bound    pvvol-1   1Gi        RWX                           21s


$ kubectl describe pvc pvc-one
Name:          pvc-one
Namespace:     default
StorageClass:
Status:        Bound
Volume:        pvvol-1
Labels:        <none>
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      1Gi
Access Modes:  RWX
VolumeMode:    Filesystem
Used By:       <none>
Events:        <none>

```

**10) PV が利用されているかどうかを確認するため、PV の状態を再確認しましょう。STATUS がBound になっているはずです。**
```
$ kubectl get pv
NAME      CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM             STORAGECLASS   REASON   AGE
pvvol-1   1Gi        RWX            Retain           Bound    default/pvc-one                           5m27s
```

**11) PVをdescribeでも見ておきましょう。**
```
$ kubectl describe pv pvvol-1
Name:            pvvol-1
Labels:          <none>
Annotations:     pv.kubernetes.io/bound-by-controller: yes
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:
Status:          Bound
Claim:           default/pvc-one
Reclaim Policy:  Retain
Access Modes:    RWX
VolumeMode:      Filesystem
Capacity:        1Gi
Node Affinity:   <none>
Message:
Source:
    Type:      NFS (an NFS mount that lasts the lifetime of a pod)
    Server:    k8scp
    Path:      /opt/sfw
    ReadOnly:  false
Events:        <none>
```

**12) PVC を利用する新しいDeployment を作成しましょう。コンテナの/optにpvvol-1をマウントします。**
```
$ vi nfs-deploy.yaml
```

[nfs-deploy.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/nfs-deploy.yaml)


**13) Deploymentを作成しましょう。**
```
$ kubectl create -f nfs-deploy.yaml
deployment.apps/nginx-nfs created
```

**14) Pod の詳細を確認しましょう。Persistent Volumeがマウントされ使用可能となっています。**
```
$ kubectl get pods
NAME                         READY   STATUS    RESTARTS   AGE
nginx-nfs-55d5d6bcc7-42hrp   1/1     Running   0          39s


$ kubectl describe pod nginx-nfs-<Tab>
Name:             nginx-nfs-55d5d6bcc7-42hrp
Namespace:        default
Priority:         0
Service Account:  default
Node:             set99-worker/10.0.0.37
...
    Mounts:
      /opt from nfs-vol (rw)
...
Volumes:
  nfs-vol:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  pvc-one
    ReadOnly:   false
...
```

**15) PVC の状態をdescribeで確認しましょう。**
```
$ kubectl describe pvc pvc-one
Name:          pvc-one
Namespace:     default
StorageClass:
Status:        Bound
Volume:        pvvol-1
Labels:        <none>
Annotations:   pv.kubernetes.io/bind-completed: yes
               pv.kubernetes.io/bound-by-controller: yes
Finalizers:    [kubernetes.io/pvc-protection]
Capacity:      1Gi
Access Modes:  RWX
VolumeMode:    Filesystem
Used By:       nginx-nfs-55d5d6bcc7-42hrp
Events:        <none>

```

**16) PVとPVCのステータスを確認しながらDeployment、PVC、PVを削除しましょう。**
```
$ kubectl delete deployments.apps nginx-nfs
$ kubectl describe pvc pvc-one
$ kubectl describe pv pvvol-1
$ kubectl delete pvc pvc-one
$ kubectl describe pv pvvol-1
$ kubectl delete pv pvvol-1

```


<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Lab5-4.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-127)    
