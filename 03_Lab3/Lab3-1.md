# 3章 APIserver

**時間:20分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Kubectlコマンドを使ったリソース操作  
・kube/configファイルの理解  


**Notices:**

・Kubernetesはインストール済みです。  
・使用するUbuntuサーバーのログイン方法はインストラクターから提供します。  

<br>
<br>


## Task1: kubectl CLIの確認

**1) 各自アサインされたユーザーでサーバーにログインします**

**2) kubectl CLIをいくつか確認しましょう。\<Tab\>を使うとコマンド補完が使えるので確認してみてください。**

```
$ kubectl get pods
No resources found in default namespace.

$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   calico-kube-controllers-6c99c8747f-d68hg   1/1     Running   0          3m51s
kube-system   calico-node-7cmgs                          1/1     Running   0          3m51s
kube-system   calico-node-9xfbs                          1/1     Running   0          3m51s
...

$ kubectl get pods -n kube-system
NAME                                       READY   STATUS    RESTARTS   AGE
calico-kube-controllers-6c99c8747f-d68hg   1/1     Running   0          4m49s
calico-node-7cmgs                          1/1     Running   0          4m49s
calico-node-9xfbs                          1/1     Running   0          4m49s
coredns-5d78c9869d-45zm7                   1/1     Running   0          5m33s
coredns-5d78c9869d-kksg7                   1/1     Running   0          5m33s
...

$ kubectl -n kube-system get pods kube-apiserver-<Tab>
NAME                          READY   STATUS    RESTARTS   AGE
kube-apiserver-set99-master   1/1     Running   0          6m33s

$ kubectl -n kube-system describe pod kube-apiserver-<Tab>
Name:                 kube-apiserver-set99-master
Namespace:            kube-system
Priority:             2000001000
Priority Class Name:  system-node-critical
Node:                 set99-master/10.0.0.253
Start Time:           Mon, 27 Nov 2023 13:36:57 +0900
Labels:               component=kube-apiserver
                      tier=control-plane
...
```
<br>
<br>

## Task 2: マニフェストを使ったPodの作成
  
**1) Podマニフェストを作成します。**

```
$ vi pod.yaml

```
[pod.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/03_Lab3/pod.yaml)


**2) Podを作成して確認しましょう。**

```
$ kubectl create -f pod.yaml
pod/nginx created

$ kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          19s
```

-o wideでPodのIPアドレスなどを確認できます。  
```
$ kubectl get pods -o wide
NAME    READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED NODE   READINESS GATES
nginx   1/1     Running   0          90s   192.168.215.130   set99-worker   <none>           <none>

```

-o yamlでPodの設定をyaml形式で表示します。  
```
$ kubectl get pods -o yaml
apiVersion: v1
items:
- apiVersion: v1
  kind: Pod
  metadata:
    annotations:
...

```

describeでPodの詳細情報を表示します。  
```
$ kubectl describe pod nginx
apiVersion: v1
items:
- apiVersion: v1
  kind: Pod
  metadata:
    annotations:
...
```

Podを削除します。  
```
$ kubectl delete pod nginx
pod "nginx" deleted
```

**3) 今度はCLIでPodを作成しましょう。**

```
$ kubectl run --image=nginx nginx
pod/nginx created

$ kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          23s

Podは残しておきます。
```

<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/03_Lab3/Lab3-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-127)  

