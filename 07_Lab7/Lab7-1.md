# 7章 Ingress

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Ingress Controllerを理解する  
・Ingressリソースを作成する


<br>
<br>


## Task1: Ingress Controllerのインストール

この演習ではNginx Ingress Controllerをインストールします。一般的なインストールはhelmを使うので最初にhelmをインストールします。  
<br>
<br>
**1) helmをインストールします。**
```
$ wget https://get.helm.sh/helm-v3.9.2-linux-amd64.tar.gz

--2023-11-28 15:33:47--  https://get.helm.sh/helm-v3.9.2-linux-amd64.tar.gz
Resolving get.helm.sh (get.helm.sh)... 152.199.39.108, 2606:2800:247:1cb7:261b:1f9c:2074:3c
Connecting to get.helm.sh (get.helm.sh)|152.199.39.108|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 13998934 (13M) [application/x-tar]
Saving to: ‘helm-v3.9.2-linux-amd64.tar.gz’

helm-v3.9.2-linux-amd64.tar.gz             100%
[======================================================================================>]  13.35M  --.-KB/s    in 0.09s

2023-11-28 15:33:47 (148 MB/s) - ‘helm-v3.9.2-linux-amd64.tar.gz’ saved [13998934/13998934]


$ tar -xvf helm-v3.9.2-linux-amd64.tar.gz
linux-amd64/
linux-amd64/helm
linux-amd64/LICENSE
linux-amd64/README.md


$ sudo cp linux-amd64/helm /usr/local/bin/helm

```

helmがインストールされhelmコマンドが使えるようになりました。  

**2) Nginx Ingress controllerリポジトリを追加してupdateします。**
```
$ helm repo add nginx-stable https://helm.nginx.com/stable
"nginx-stable" has been added to your repositories


$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "nginx-stable" chart repository
Update Complete. ?Happy Helming!?
```

**3) Ingress controllerをインストールします。まずIngress controller用のNamespaceを作成します。**
```
$ kubectl create ns ingress
namespace/ingress created
```
**4 ) ingress namespaceにIngress controllerをインストールします。**
```
$ helm install -n ingress nginx-ingress nginx-stable/nginx-ingress
NAME: nginx-ingress
LAST DEPLOYED: Tue Nov 28 15:36:55 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The NGINX Ingress Controller has been installed.
```

**4) Ingress Controller PodとServiceを確認します。**
```
$ kubectl -n ingress get deployments,pods
NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-ingress-controller   1/1     1            1           4m21s

NAME                                            READY   STATUS    RESTARTS   AGE
pod/nginx-ingress-controller-56887b46bb-bwhsm   1/1     Running   0          4m21s


$ kubectl -n ingress get svc
NAME                       TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)                      AGE
nginx-ingress-controller   LoadBalancer   10.103.69.7   <pending>     80:31223/TCP,443:31203/TCP   5m1s
```

Ingress controllerがインストールされました。


<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/07_Lab7/Lab7-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-127)    



